#include <chrono>
#include <iostream>
#include <type_traits>
#include <random>
#include <args.hxx>

//#include "../papi-wrapper/src/papi_wrapper.h"

//#include <atomic>

#define COUNT size_t(100000000)
#define SIZE (size_t(0b1) << 30)
#define WALK_LENGTH (size_t(0b1) << 24)
#define CACHE_SIZE ((size_t(1) << 23) *5)

#define CACHE_LINESIZE 64
//#define CACHE_LINESIZE 128
//#define CACHE_LINESIZE 2048

#include <cstring>

struct Field {
	Field *nextField/* = std::rand() % SIZE*/;
	Field *randomPermutation;
	size_t hit = 0;
	size_t hitCheck = 0;
	size_t rest[(CACHE_LINESIZE / sizeof (size_t)) -4];
};

void fillArray(Field *arr, const size_t size) {
	//init array linear
	for (size_t i=0; i<size; ++i) {
		arr[i].randomPermutation = arr + i;
	}

	//permute pointers
	for (size_t i=0; i<size; ++i) {
		size_t random = i + (std::rand() % (size - i));
		std::swap(arr[i].randomPermutation, arr[random].randomPermutation);
	}

	//cycle free permutation
	Field *nextField = arr[size -1].randomPermutation;
	for (size_t i=0; i<size; ++i) {
		nextField->nextField = arr[i].randomPermutation;
		nextField = nextField->nextField;
	}
}

uint64_t read_pmc(int ecx)
{
	unsigned int a, d;
	__asm __volatile("rdpmc" : "=a"(a), "=d"(d) : "c"(ecx));
	return ((uint64_t)a) | (((uint64_t)d) << 32);
}

/**
 * @brief measureLookup
 * @param nextField
 * @param walkLength
 * @return time in nano seconds
 */
std::chrono::nanoseconds measureLookup(Field *nextField, const size_t walkLength) {

	std::cout << "lookup count (Elements): " << walkLength << std::endl;
	std::cout << "starting lookup" << std::endl;

	auto start = std::chrono::high_resolution_clock::now();
	asm volatile("NOP #lookup_begin");
	for (size_t i=0; i<walkLength; ++i) {
//		asm volatile("NOP #update_nextField_begin");
		nextField = nextField->nextField;
//		asm volatile("NOP #update_nextField_end");
	}
	asm volatile("NOP #lookup_end");
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << "dummy value: " << nextField << nextField->rest << std::endl;
	return std::chrono::nanoseconds(end - start);
}

void checkCycles(Field *arr, const size_t size) {
	std::cout << "checking for cycles..." << std::endl;
	size_t foundCycles = 0;
	Field *nextField = arr;
	for (size_t i=0; i<size; ++i) {
		if (nextField->hitCheck) {
			++foundCycles;
		}
		nextField->hitCheck = 1;
		nextField = nextField->nextField;
	}
	std::cout << "found " << foundCycles << " cycles." << std::endl;
}

void clearCache(size_t cacheSize) {
	std::cout << "clearing cache" << std::endl;
	size_t *cacheClearArr = new size_t[cacheSize / sizeof (size_t)];
	for (size_t i=0; i < (cacheSize / sizeof (size_t)); ++i) {
		cacheClearArr[i] = std::rand();
	}
	for (size_t i=1; i < (cacheSize / sizeof (size_t)); ++i) {
		cacheClearArr[i] = cacheClearArr[i-1];
	}
	std::cout << "dummy output of last cache clearing: " << cacheClearArr[cacheSize / sizeof (size_t)-1] << std::endl;
	delete[] cacheClearArr;
}

int main(int argc, char **argv) {
	typedef Field ElemT;
	std::random_device randomDevice;

	args::ArgumentParser parser("This is a kmer tool to store kmers in a hashtable.", "Master thesis project of Uriel Elias Wiebelitz");
	args::HelpFlag help(parser, "help", "display this help menu", {'h', "help"});
	args::CompletionFlag completion(parser, {"complete"});
	args::ValueFlag<size_t> size(parser, "size", "size of array in Bytes", {'s'}, SIZE);
	args::ValueFlag<size_t> walkLength(parser, "walk length", "number of steps to iterate through the array", {'w'}, WALK_LENGTH);
	args::ValueFlag<size_t> cacheSize(parser, "cache size", "size of the cache for wiping", {"cs"}, CACHE_SIZE);
	args::ValueFlag<size_t> seed(parser, "cache size", "size of the cache for wiping", {"seed"}, randomDevice());
	args::ValueFlag<bool> checkCyclesInArr(parser, "check cycles", "check for cycles in walk", {"cC"}, false);

	try {
		parser.ParseCLI(argc, argv);
	}
	catch (const args::Completion& e) {
		std::cout << e.what();
		return 0;
	}
	catch (const args::Help&) {
		std::cout << parser;
		return 0;
	}
	catch (const args::ValidationError &e) {
		std::cerr << e.what() << std::endl << std::endl;
		std::cerr << parser;
		return 0;
	}
	catch (const args::ParseError &e) {
		std::cerr << e.what() << std::endl;
		std::cerr << parser;
		return 0;
	}

	std::srand(seed.Get());

	size_t elemCount = size.Get() / sizeof (ElemT);

	std::cout << "elem size (Bytes): " << sizeof(ElemT) << std::endl;
	std::cout << "array size (Bytes): " << elemCount * sizeof (ElemT) << std::endl;
	std::cout << "array size (Elements): " << elemCount << std::endl;

	ElemT *arr = new ElemT[elemCount];
//	if ((size_t(arr) % 64) != 0) {
//		std::cerr << "adress is " << arr << std::endl;
//		throw "the cache is not aligned!";
//	}

	fillArray(arr, elemCount);
	if (checkCyclesInArr.Get()) {
		checkCycles(arr, elemCount);
	}
	clearCache(cacheSize.Get());
	auto duration = measureLookup(arr, walkLength.Get());
	delete[] arr;

	std::cout << "lookup time (sec): "
			  << std::chrono::duration_cast<std::chrono::duration<double>>(duration).count() << std::endl;
	std::cout << "time per lookup (nanosec): " << std::chrono::duration_cast<std::chrono::duration<double, std::nano>>(duration).count() / walkLength.Get() << std::endl;
}
