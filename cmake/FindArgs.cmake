message(STATUS)
message(STATUS "Trying to locate package: Args")

# Temporarily disable REQUIRED if set
set(Args_FIND_REQUIRED_BACKUP ${Args_FIND_REQUIRED})
set(Args_FIND_REQUIRED_BACKUP 0)

set(Args_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/Args/")

find_path(Args_INCLUDE_DIRS args.hxx
        PATHS ${Args_INSTALL_DIR} /usr/local/args /opt/args)

set(Args_FIND_REQUIRED ${Args_FIND_REQUIRED_BACKUP})

if (NOT ${Args_INCLUDE_DIRS} MATCHES "Args_INCLUDE_DIRS-NOTFOUND")
    message(STATUS "Found Args-Library")
else ()
    message(STATUS "Could NOT locate Args on system -- preparing download")
endif ()
