OUT_PATH = config["out_path"]
BUILD_PATH = config["build_path"]
SRC_PATH = config["src_path"]

workdir: config["work_dir"]


import random
random.seed(75)

SIZES_PREF = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,32,64,128,256,384,512,640,768,896,]

SIZES_KB = [str(x) + "KB" for x in ([x for x in range(1,32)] + [32,48,64,80,96,112] + [128 + x*32 for x in range(28)])]
SIZES_MB = [str(x) + "MB" for x in ([x for x in range(1,32)] + [16,32,48,64,80,96,112,128,256,384,512,640,768,896])]
SIZES_GB = [str(x) + "GB" for x in ([x for x in range(1,16)] + [16, 18, 20, 22, 24, 26, 28, 30, 32,40,48,56])]

SIZES = SIZES_KB + SIZES_MB + SIZES_GB

SEEDS = [random.randrange(1,1000000000000) for _ in range(1000)]

WL = [10**9]

CONFIGS = expand(OUT_PATH + "mem_size:{size}.walkLength:{wL}/run:{run}.txt"
    , size=SIZES, wL=WL, run=range(20))

rule all:
    input:
        CONFIGS

def sizeToByte(sizeStr):
    multiplicator = 1
    
    if sizeStr.endswith("KB"):
        multiplicator = 1024**1
    if sizeStr.endswith("MB"):
        multiplicator = 1024**2
    if sizeStr.endswith("GB"):
        multiplicator = 1024**3
        
    return int(sizeStr[:-2]) * multiplicator


rule run:
    output:
        OUT_PATH + "mem_size:{mem}.walkLength:{walkLength}/run:{run}.txt"
    input:
        BUILD_PATH + "MemLookup_Bench"
    params:
        memBytes = lambda wildcards: str(sizeToByte(wildcards.mem)),
        seed = lambda wildcards: str(SEEDS[int(wildcards.run)])
    shell:
        """
        {input[0]} -s {params.memBytes} -w {wildcards.walkLength} --seed {params.seed} > '{output}'
        """
        
rule compile:
    output:
        BUILD_PATH + "MemLookup_Bench"
    input:
        SRC_PATH
    shell:    
        """
        mkdir -p {BUILD_PATH}
        cd {BUILD_PATH}
        cmake {input[0]} -DCMAKE_BUILD_TYPE=Release
        make -j9
        """
        
